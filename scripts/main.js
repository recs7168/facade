$(document).ready(function() {
	// logger url copy
	$('.copy-icon').on('click', function() {
		copyToClipboard($('.logger-link'));
		$('.copy-tooltip').addClass('visible');
		setTimeout(function() {
			$('.copy-tooltip').removeClass('visible');
		}, 1000);
	});
	// youtube url input
	$(".topbar input").keyup(function(event) {
	    if (event.keyCode == 13) {
	        submitRequest($(this).val());
	    }
	});
	// ip info close
	$(".ip.info .close-icon").on('click', function() {
		$('.ip.info').css('display', 'none');
	});
	// refresh button
	$(".log.info .refresh-icon").on('click', function() {
		submitRequest($(".topbar input").val());
	});
	// clear logs button
	$(".log.info .trash-icon").on('click', function() {
		curr_logger.clearLogs();
		submitRequest($(".topbar input").val());
	});
});