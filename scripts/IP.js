class IP {
	constructor(address) {
		this.address = address;
		this.grabGeolocation();
	}

	// geolocation through GeoIP through WebResolver
	grabGeolocation(success=function(){}, failure=function(){}) {
		let self = this;
		$.getJSON(`https://webresolver.nl/api.php?key=8587Q-39V39-Z6DUE-EJRR0&json&action=geoip&string=${self.address}`, function(data) {
			if (data.ip == self.address && data.success) {
				success(data);
			} else {
				failure(data);
			}
 		});
	};

	// geolocation through IPAPI
	// grabGeolocation(success=function(){}, failure=function(){}) {
	// 	let self = this;
	// 	$.getJSON(`http://ip-api.com/json/${self.address}?callback=?`, function(data) {
	// 		if (data.status == "success" && self.address == data.query) {
	// 			self.country = data.country;
	// 			self.country_code = data.countryCode;
	// 			self.region = data.regionName;
	// 			self.region_code = data.region;
	// 			self.city = data.city;
	// 			self.zip = data.zip;
	// 			self.latitude = data.lat;
	// 			self.longitude = data.lon;
	// 			self.isp = data.isp;
	// 			self.organization = data.org;
	// 			self.asn = data.as;
	// 			self.timezone = data.timezone;
	// 			success(self);
	// 		} else {
	// 			failure(self);
	// 		}
	// 	});
	// };
}