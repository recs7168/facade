var curr_logger;
// function that allows access to the clipboard
function copyToClipboard(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
}

function submitRequest(youtube_url) {
	// start preloader
	$('.log.info table').remove();
	$('.log.info p').remove();
	$('.content-wrapper').css('visibility', 'hidden');
	$('.content-wrapper > .preloader').css('visibility', 'visible');
	// create IPLogger
	curr_logger = new IPLogger(youtube_url);
	$('.logger-link').text(curr_logger.logger_url);
	// get logs
	curr_logger.getLogs(function(response) {
		let data = $(response).find("table");
		if (data.length > 0) {
			$('.log.info').append(data);
			$('.log.info tbody tr').on('click', function() {
				console.log('clicked');
				let address = $(this).children().first().text();
				console.log(address);
				ipToLocation(address);
			});
		} else {
			$('.log.info').append($('<p>No data to show.</p>'));
		}
		// remove preloader
		$('.content-wrapper').css('visibility', 'visible');
		$('.content-wrapper > .preloader').css('visibility', 'hidden');
	});
}
// grabds geolocation from ip and adds to DOM
function ipToLocation(address) {
	let ip = new IP(address);
	$('.ip.info').css('display', 'block');
	$('.ip.info > .preloader').css('visibility', 'visible');
	$('.ip.info table').css('visibility', 'hidden');
	ip.grabGeolocation(success=function(response) {
		console.log(response);
		// add response to the DOM
		$('.ip.info .ip td').html(response.ip);
		$('.ip.info .hostname td').html(response.hostname);
		$('.ip.info .area-code td').html(response.records.area_code);
		$('.ip.info .city td').html(response.records.city);
		$('.ip.info .region td').html(response.records.region.name);
		$('.ip.info .country td').html(response.records.country_name);
		$('.ip.info .zip td').html(response.records.postal_code);
		$('.ip.info .coordinates td').html(response.records.latitude + ", " + response.records.longitude);
		$('.ip.info .organization td').html(response.records.organization);

		$('.ip.info > .preloader').css('visibility', 'hidden');
		$('.ip.info table').css('visibility', 'visible');
	});
}